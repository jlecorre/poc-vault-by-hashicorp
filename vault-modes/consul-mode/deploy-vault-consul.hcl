# configuration du backend de stockage
storage "consul" {
  address = "consul:8300"
  path = "vault"
  scheme = "http"
  redirect_addr = "http://0.0.0.0:8300"
}

# configuration du listener API
listener "tcp" {
  address = "vault:8400"
  tls_disable = 1
}

# configuration globale
# default_lease_ttl: "168h"
# max_lease_ttl: "720h"
# interdire le dump du swap sur disque
# disable_mlock: true # désactivé pour les tests
