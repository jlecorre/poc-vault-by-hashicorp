# configuration du backend de stockage
storage "file" {
  path = "/vault/data"
}

# configuration du listener API
listener "tcp" {
  address = "vault-file-mode:8200"
  tls_disable = 1
}

# interdire le dump du swap sur disque
disable_mlock = "false"

# configuration globale
default_lease_ttl = "168h"
max_lease_ttl = "720h"
