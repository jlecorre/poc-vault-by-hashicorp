# ACL liées à l'environnement de développement

path "generic/ops/*" {
    policy = "read"
}

path "generic/dev/*" {
    policy = "read"
}
