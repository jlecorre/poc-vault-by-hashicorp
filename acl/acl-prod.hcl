# ACL liées à l'environnement de production

path "generic/ops/*" {
    capabilities = [ "list", "read", "update", "delete" ]
    policy = "write"
}

path "generic/dev/*" {
    capabilities = [ "deny" ]
}

path "auth/token/lookup-self" {
    policy = "read"
}
