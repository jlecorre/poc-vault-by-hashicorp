# Manipuler une instance VAULT by HashiCorp

## Informations supplémentaires
* Ce dépôt de code servira de POC (aka proof of concept) et de mémo pour tout ce qui concerne l'utilisation d'une instance Vault
* Vault est un produit HashiCorp => https://www.vaultproject.io (version open source)
* Image Docker officielle utilisée pour la R&D => https://hub.docker.com/r/_/vault/
* Image Docker officielle Consul pour la version Vault "prod" => https://hub.docker.com/r/_/consul/

## Liste des commandes exécutables

### Manipuler le conteneur Docker dédié à l'instance Vault
* Démarrage d'un conteneur vault en mode 'développement' :  
`docker run -p 8200:8200 --cap-add=IPC_LOCK -e 'SKIP_SETCAP="1"' -e 'VAULT_DEV_ROOT_TOKEN_ID=admin' -d --name=dev-vault vault`
* Consulter les logs du conteneur Vault :  
`docker container logs dev-vault`

Exemple de logs indiquant que Vault est démarré en mode 'développement' :  
```
==> WARNING: Dev mode is enabled!

In this mode, Vault is completely in-memory and unsealed.
Vault is configured to only have a single unseal key. The root
token has already been authenticated with the CLI, so you can
immediately begin using the Vault CLI.
```

### Récupérer le client vault en local
* Consulter les versions disponibles de l'outil : https://releases.hashicorp.com/vault/
* Sélectionner la version cible
* Exécuter les commandes suivantes :
```
sudo mkdir -p /opt/vault; cd /opt/vault;
export vault_version="0.8.1"
sudo wget https://releases.hashicorp.com/vault/"$vault_version"/vault_"$vault_version"_linux_amd64.zip
sudo unzip vault_"$vault_version"_linux_amd64.zip
```

### Exporter les variables d'environnement
Cela permet de communiquer avec le serveur Vault une fois l'instance démarrée.  
`export VAULT_ADDR='http://0.0.0.0:8200'`

### Initialiser une instance Vault
Cela génère les clefs à sauvegarder pour sceller/désceller l'instance Vault.  
`./vault init`

### S'identifier à une instance Vault
`./vault auth`

### Consulter le statut d'une instance Vault
`./vault status`

Exemple :  
```
Sealed: false
Key Shares: 1
Key Threshold: 1
Unseal Progress: 0
Unseal Nonce: 
Version: 0.7.3
Cluster Name: vault-cluster-352140cc
Cluster ID: 8624a224-3140-1f75-bc4a-083469fd6493

High-Availability Enabled: false
```

Un fonctionnement en mode cluster peut être activable.

### Lister les backends Vault disponibles
`./vault mounts`

Exemple :  
```
Path        Type       Accessor            Plugin  Default TTL  Max TTL  Force No Cache  Replication Behavior  Description
cubbyhole/  cubbyhole  cubbyhole_6dc11f29  n/a     n/a          n/a      false           local                 per-token private secret storage
secret/     generic    generic_1a0f362a    n/a     system       system   false           replicated            generic secret storage
sys/        system     system_eadbb4f7     n/a     n/a          n/a      false           replicated            system endpoints used for control, policy and debugging
```

### Monter un nouveau backend
Un __backend__ est un composant Vault qui permet le stockage des secrets ([liste des secrets disponibles](https://www.vaultproject.io/docs/secrets/index.html))  
Les backends sont isolés les uns des autres, il peut donc être envisageable d'avoir un backend par équipe.  
`./vault mount -description "mon super backend de poc" generic"`

Exemple :  
```
Successfully mounted 'generic' at 'generic'!
```

Le backend est visible si on liste à nouveau les backends disponibles :  
```
Path        Type       Accessor            Plugin  Default TTL  Max TTL  Force No Cache  Replication Behavior  Description
generic/    generic    generic_9afd86a5    n/a     system       system   false           replicated            mon super backend de poc
```

### Ajouter une entrée dans un backend
`./vault write backend_name/secret_name value=value`

Exemple :  
`./vault write generic/hello value=magical_password`

### Lister les entrées disponibles dans un backend
`./vault list backend_name`  

Exemple :  
```
./vault list generic

Keys
----
hello
test
poc1
poc2
yeaaaah
```

### Lire une entrée dans un backend
`./vault read backend_name/secret_name`

Exemple :  
```
./vault read generic/hello

Key                     Value
---                     -----
refresh_interval        768h0m0s
value                   magical_password
```

La CLI Vault prend en compte le formattage JSON de l'output de la fonctionnalité __read__.  
Cela permet de filtrer la sotie avec des outils comme JQ ou autres :  
`./vault read -format=json generic/hello | jq [-r] .data.value`

### Visualiser le statut d'un token utilisateur
`./vault token-lookup username`

Exemple :  
```
./vault token-lookup admin

Key                     Value
---                     -----
accessor                d3eda59f-a2f6-2ee2-2947-eb4711a83c83
creation_time           1504091194
creation_ttl            0
display_name            token
expire_time             <nil>
explicit_max_ttl        0
id                      admin
issue_time              2017-08-30T11:06:34.833414492Z
meta                    <nil>
num_uses                0
orphan                  true
path                    auth/token/create
policies                [root]
renewable               false
ttl                     0
```

### Obtenir de l'aide sur un backend
`./vault path-help backend_name/entry_type/secret_name`

Exemple :  
```
./vault path-help generic/hello

Request:        hello
Matching Route: ^.*$

Pass-through secret storage to the storage backend, allowing you to
read/write arbitrary data into secret storage.

## DESCRIPTION

The pass-through backend reads and writes arbitrary data into secret storage,
encrypting it along the way.

A TTL can be specified when writing with the "ttl" field. If given, the
duration of leases returned by this backend will be set to this value. This
can be used as a hint from the writer of a secret to the consumer of a secret
that the consumer should re-read the value before the TTL has expired.
However, any revocation must be handled by the user of this backend; the lease
duration does not affect the provided data in any way.
```

### Supprimer une entrée
`./vault delete backend_name/secret_name`

## Gestion des authentifications
__Autentification__ = attribution d'une identité à un utilisateur  
Les systèmes d'autentification sont "pluggables" comme les backends de stockage des secrets.  
Exemple de backends AUTH : Tokens, SSH, Github Backend, LDAP, etc.  

### Création d'un backend de type Tokens
`./vault token-create -display-name="token-poc-magique" -ttl="4h"`

Exemple : 
```
Key             Value
---             -----
token           53b5f9a0-94b8-7a6e-0171-2aa4894c42a2
token_accessor  d579cb66-93de-378c-ea23-8c04794eabb1
token_duration  4h0m0s
token_renewable true
token_policies  [root]
```

Un token hérite par défaut de la politique de sécurité du token parent qui l'a généré.  
Si j'appartiens au groupe `root` alors le token créé appartient au groupe `root` !  
Un token a toujours un *parent*. Si un token *parent* est révoqué, les tokens *enfant* le sont également.  

### S'authentifier avec un token
`./vault auth token_id`

### Révoquer un token
`./vault token-revoke token_id`

Exemples :  
```
./vault token-revoke bfc3435e-5112-8c25-e64e-cc4ca697b9c1`
./vault token-revoke -mode=path auth/github`
```

### Désactiver un backend d'authentification
`vault auth-disable github`

## Gestion des autorisations
__Autorisations__ = attribution des autorisations d'accès et des permissions à une identité
Le format des autorisations est le même quelque soit le auth-backend utilisé !  
Les permissions `root` ne peuvent pas être supprimées d'un auth-backend.  
Une identité liée au groupe `root` à tous les droits sur le groupe.  
Le format des politiques d'accès est HCL (Human Readable Configuration)(json-compatible).  
Tout ce qui n'est pas autorisé dans la politique HCL est par défault interdit !  

### Exemple de fichiers de configuration
Pour plus d'informations, voir la page : [Concept des politique de sécurité](https://www.vaultproject.io/docs/concepts/policies.html)
```
path "secret-backend/*" {
  policy = "write"
}

path "secret-backend/foo" {
  policy = "read"
}

path "auth/token/lookup-self" {
  policy = "read"
}
```

### Ecrire une politique de groupe
Un exemple de politique de groupe est [disponible ici](./acl/acl-dev.hcl).  
Après avoir sauvegarder ce fichier définissant les règles de notre politique de sécurité dans votre
environnement, il est possible d'activer une politique de sécurité :  
`./vault policy-write policy_name file_name.hcl`

Exemple :  
```
./vault policy-write magical_poc ./acl/acl-dev.hcl`

Policy 'magical_poc' written.
```

### Visualiser les politiques de sécurité (root only)
```
./vault policies
./vault policies policy_name
```

### Création d'un token pour une politique de sécurité donnée
`./vault token-create -policy="secret"`

### Mapper une politique de sécurité à un secret-backend
Tout backend d'autentification doit être mappé à une politique de sécurité.  
`./vault write auth/github/map/teams/default value=secret`  
Dans cet exemple, la politique de sécurité "secret" est liée au backend d'autentification github.  

## Vault en mode instance de production

### Démarrage d'une instance Vault dockerisée de production
#### Vault en mode 'fichier'
Pour démarrer une instance Vault qui hébergera ses données dans des fichiers plats disponibles sur la machine hôte :  
`docker-compose -f docker-compose/vault-prod-file-mode.yml up -d`  
Pour visualiser les logs de l'instance fraîchement démarrée :  
`docker-compose -f docker-compose/vault-prod-file-mode.yml logs -f`

#### Vault en mode 'consul'
Pour démarrer une instance Vault qui hébergera ses données dans un backend consul dédié :  
`docker-compose -f docker-compose/vault-prod-consul-mode.yml up -d`  
Pour visualiser les logs de l'instance fraîchement démarrée :  
`docker-compose -f docker-compose/vault-prod-consul-mode.yml logs -f`

### Démarrage d'une instance Vault (mode binaire) de production
Au démarrage du serveur, Vault est scellé.  
Il faudra alors le désceller pour ajouter/consulter les données qu'il hébergera.  
`./vault server -config=example.hcl`

### Initialisation du serveur Vault
```
export VAULT_ADDR='http://0.0.0.0:8200'
./vault init
```

### Manipuler une instance Vault de production
Pour sceller/désceller une instance Vault, il faut au minimum 3 clefs de déchiffrement.  
Celles-ci sont fournies par le serveur lors de la 1ère initialisation. Attention à ne pas perdre ces informations critiques !  

Exemple de l'affichage utilisateur de l'initialisation d'une instance Vault :  
```
Unseal Key 1: RBKT4mHyYv+1ih/nEReb1L+cb34wxhsYg3NtYknCAlqg
Unseal Key 2: W+600MV55uwA02UHoWvfqi45X77RQq2q3eF1Urki3FvC
Unseal Key 3: QWwkP7fkhgVzFlB8NyCcD8L6X4vmAamjxIy2tilcwuNl
Unseal Key 4: 1Atfws/cWU/gV8EFRO+gX+7/wQ8Ic8sn4AVnxl4EUMP/
Unseal Key 5: oRJQGv5S5i81RzwowWqrkjLOc2FWhnGq+KiwOLSD++q5
Initial Root Token: 7e0e5e3c-cfa8-f8ad-adbf-23892ef1e2bb

Vault initialized with 5 keys and a key threshold of 3. Please
securely distribute the above keys. When the vault is re-sealed,
restarted, or stopped, you must provide at least 3 of these keys
to unseal it again.

Vault does not store the master key. Without at least 3 keys,
your vault will remain permanently sealed.
```

### Chiffrer / Déchiffrer une instance Vault
```
./vault seal
./vault unseal
```
Pour déchiffrer une instance Vault, il faut exécuter la commande `unseal` 3 fois de suite en lui fournissant une clef de déchiffrement différente à chaque appel de la CLI.  

Exemple de logs générés lors du déchiffrement d'une instance Vault :  
```
Key (will be hidden): 
Sealed: true
Key Shares: 5
Key Threshold: 3
Unseal Progress: 2
Unseal Nonce: 955c4ed3-cd19-cde9-452a-0447cd91198a
```

Une fois Vault déchiffré (aka unseal dans la documentation), il est possible de se logguer : `./vault auth`  

## docker-compose + fichiers de configuration

* Pour consulter les fichiers `docker-compose` qui permettent de démarrer une instance Vault en mode fichier ou avec consul comme backend : [docker-compose](./docker-compose/) 
* Pour consulter les fichiers de configuration, c'est par ici que cela se passe : [fichiers de configuration](./vault-modes/)

## Manipuler l'API d'une instance Vault
Les commandes disponibles via la CLI appellent en réalité l'API HTTP du serveur Vault.  
La documentation de l'API se trouve ici : https://www.vaultproject.io/api/index.html  
Une fois le vault descéllé, toute opération via l'API nécessite un token d'authentification (X-Vault-Token HTTP header)  

### Récupérer l'IP du Vault dans un conteneur Docker
`docker container inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name`

### Vérifier le statut d'une instance Vault via l'API HTTP
```
curl 0.0.0.0:8400/v1/sys/init` # ici notre conteneur expose l'IP `0.0.0.0`
```

### Consulter les informations d'un secret via l'API REST
Toute consultation de l'API nécessite un token d'authentification.  
Ici on le fait passer via un `Header` propre à Vault et on active le mode 'silence' de curl pour ne pas polluer l'output de la CLI exécutée.  
Ici le token root est utilisé pour l'exemple, mais il faut penser à générer un token utilisateur dédié.  
On utilise JQ pour formatter l'output de l'API :  
`curl -s -X GET -H "X-Vault-Token: 0137fda2-8beb-f8b7-384b-66b7e23fc552 0.0.0.0:8200/v1/secret/jlecorre | jq -r .`

### Ecrire un secret via l'API 
Il faut ajouter à notre requête HTTP un corps définissant les valeurs à ajouter à notre backend de stockage.  
`curl -s -H "Content-Type: application/json" -X POST -H "X-Vault-Token: 0137fda2-8beb-f8b7-384b-66b7e23fc552" -d '{"value":"strong_password_or_not"}' 0.0.0.0:8400/v1/secret/coin`

### Obtenir de l'aide à propos d'une méthode de l'API
Il suffit d'ajouter `?help=1` à n'importe quelle requête faite sur l'API :  
`curl -s -X GET -H "X-Vault-Token: 0137fda2-8beb-f8b7-384b-66b7e23fc552" "0.0.0.0:8400/v1/secret?help=1" | jq .`

## Vault et LDAP
Article intéressant à lire pour la mise en place d'une liaison entre un serveur Vault et un serveur LDAP :  
http://www.pugme.co.uk/index.php/2017/02/08/using-the-hashicorp-vault-ldap-auth-backend/

### Activer le backend d'authentification LDAP
`./vault auth-enable ldap`

### Lister les backends d'authentification actifs d'une instance Vault
`./vault auth -methods`

### Configuration de l'auth-backend LDAP
La configuration du backend d'authentification LDAP se fait via la CLI.  
La configuration est active en mode 'Binding - Anonymous Search'  
```
./vault write auth/ldap/config \
  url="ldap://ldap.domain.tld/" \
  starttls="true" \
  insecure_tls="true" \
  groupdn="ou=foo,ou=bar,dc=foo,dc=bar" \
  userdn="ou=foo,ou=bar,dc=foo,dc=bar" \
  userattr="uid" \
  deny_null_bind="false" \
  discoverdn="true"
```

### Récupérer la configuration de l'auth-backend LDAP
`curl -s -X GET -H "X-Vault-Token: 0137fda2-8beb-f8b7-384b-66b7e23fc552" "0.0.0.0:8400/v1/auth/ldap/config" | jq .`

### Ajouter une policy à un groupe LDAP (mapping LDAP <-> backend policy)
`./vault write auth/ldap/groups/"group_name" policies=policy_name`

### Lire les policies attribuées à un groupe LDAP
`./vault read auth/ldap/groups/"group_name"`

## Sources
https://hub.docker.com/r/_/vault/  
https://hub.docker.com/r/_/consul/  
https://www.vaultproject.io/docs/index.html  
https://www.vaultproject.io/docs/secrets/ssh/index.html  
http://www.pugme.co.uk/index.php/2017/02/08/using-the-hashicorp-vault-ldap-auth-backend/  
